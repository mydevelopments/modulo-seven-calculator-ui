const path = require('path');
const express = require('express');
const compression = require('compression');
const app = express();

const PORT = process.env.PORT || 5000;
const HOST = process.env.HOST || '0.0.0.0'; // Heroku default host.

console.info('==> Connecting to the server...');
console.info('process.env.NODE_ENV %s \n', process.env.NODE_ENV);

// Using webpack-dev-server and middleware in development environment
if (process.env.NODE_ENV !== 'production') {
  const webpackDevMiddleware = require('webpack-dev-middleware');
  const webpackHotMiddleware = require('webpack-hot-middleware');
  const webpack = require('webpack');
  const config = require('../webpack.config');
  const compiler = webpack(config);

  app.use(webpackDevMiddleware(compiler, { noInfo: true, publicPath: config.output.publicPath }));
  app.use(webpackHotMiddleware(compiler));
}

// Compress statics.
app.use(compression());

// Set cache
app.use(express.static(path.join(__dirname, '/../'), { maxAge: 4 * 60 * 60 * 1000 /* 2hrs */ }));

// Deliver the root HTML file.
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname + '/../index.html'));
});

app.listen(PORT, (error) => {
  if (error) {
    console.error(error);
  } else {
    console.info("==> Listening on port %s. Visit http://%s:%s/ in your browser.\n", PORT, HOST, PORT);
  }
});
