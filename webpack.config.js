// File: webpack.config.js
const ExtractTextPlugin = require('extract-text-webpack-plugin');

// Prefixer.
const precss = require('precss');
const autoprefixer = require('autoprefixer');

// Webapack.
const webpack = require('webpack');

// Paths
const path = require('path');
const mainPath = path.resolve(__dirname, 'src', 'index.js');
const nodeModulesPath = path.resolve(__dirname, 'node_modules');

module.exports = {
  entry: {
    bundle: './src/index.js'
  },
  output: {
    path: __dirname,
    publicPath: '/',
    filename: 'public/[name].js'
  },
  module: {
    loaders: [
      {
        exclude: nodeModulesPath,
        loader: 'babel',
        query: {
          presets: ['react', 'es2015', 'stage-1']
        }
      },
      {
        test: /\.scss$/,
        // autoprefixer-loader is now deprecated
        loader: ExtractTextPlugin.extract('css!postcss!sass')
      }
    ]
  },
  resolve: {
    extensions: ['', '.js', '.jsx']
  },
  postcss: () => {
    return [precss, autoprefixer];
  },
  devServer: {
    historyApiFallback: true,
    contentBase: './'
  },
  plugins: [
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
    new ExtractTextPlugin("public/style.css", {
      allChunks: true
    })
  ]
};
