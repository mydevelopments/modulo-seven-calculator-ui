import React, { Component} from 'react';
import PointTarget from 'react-point';

class CalculatorKey extends Component {
  render() {
    const { onPress, className, ...props } = this.props

    return (
      <PointTarget onPoint={onPress}>
        <a {...props} />
      </PointTarget>
    )
  }
}

export default CalculatorKey;
