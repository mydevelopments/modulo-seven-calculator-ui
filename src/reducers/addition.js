import {
  MAKE_ADDITION
} from '../actions/actionTypes';

const INITIAL_STATE = {};

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case MAKE_ADDITION:
      return { ...state, ...action.payload.data };
    default:
      return state;
  }
}
