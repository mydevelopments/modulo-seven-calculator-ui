import { combineReducers } from 'redux';
import additionReducer from './addition';
import subtractionReducer from './subtraction';
import multiplicationReducer from './multiplication';

const rootReducer = combineReducers({
  addition: additionReducer,
  subtraction: subtractionReducer,
  multiplication: multiplicationReducer
});

export default rootReducer;
