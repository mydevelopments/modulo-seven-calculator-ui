import {
  MAKE_SUBTRACTION
} from '../actions/actionTypes';

const INITIAL_STATE = {};

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case MAKE_SUBTRACTION:
      return { ...state, ...action.payload.data };
    default:
      return state;
  }
}
