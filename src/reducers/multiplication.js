import {
  MAKE_MULTIPLICATION
} from '../actions/actionTypes';

const INITIAL_STATE = {};

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case MAKE_MULTIPLICATION:
      return { ...state, ...action.payload.data };
    default:
      return state;
  }

  return state;
}
