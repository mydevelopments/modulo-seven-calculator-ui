import axios from 'axios';
import common from '../config/common';
import {
  MAKE_ADDITION, MAKE_SUBTRACTION, MAKE_MULTIPLICATION
} from './actionTypes';

const config = common.config();
const ROOT_URL = config.ROOT_URL;
const MODULO = config.MODULO;

/*
 * Action creator to make the sum operation between a and b with modulus MODULO.
 * parameters Numbers a, b
 * return     Promise -> JSON object { result: "3" }.
 */
export function makeAddition(a, b) {
  const request = axios.get(`${ROOT_URL}/addition/${a}/${b}/${MODULO}`);

  return {
    type: MAKE_ADDITION,
    payload: request
  };
}

/*
 * Action creator to make the subtraction operation between a and b with modulus MODULO.
 * parameters Numbers a, b
 * return     Promise -> JSON object { result: "3" }.
 */
export function makeSubtraction(a, b) {
  const request = axios.get(`${ROOT_URL}/subtraction/${a}/${b}/${MODULO}`);

  return {
    type: MAKE_SUBTRACTION,
    payload: request
  };
}

/*
 * Action creator to make the multiplication operation between a and b with modulus MODULO.
 * parameters Numbers a, b
 * return     Promise -> JSON object { result: "3" }.
 */
export function makeMultiplication(a, b) {
  const request = axios.get(`${ROOT_URL}/multiplication/${a}/${b}/${MODULO}`);

  return {
    type: MAKE_MULTIPLICATION,
    payload: request
  };
}
