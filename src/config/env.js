const Environment = {
  development: {
    ROOT_URL: 'http://0.0.0.0:8081/api/calculator',
    MODULO: 7
  },
  production: {
    ROOT_URL: 'https://moduloncalculator-rest-api.herokuapp.com/api/calculator',
    MODULO: 7
  }
};

module.exports = Environment;
