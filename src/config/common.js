import env from './env';

// Read the environment configurtion parameters from the env module.

exports.config = () => {
  const NODE_ENV = process.env.NODE_ENV || 'development';

  return env[NODE_ENV];
};
