# Module 7 Calculator UI
The UI has been devoloped using JavaScript (React), CSS3 and HTML5. Under the hood is perfoming AJAX queries using axios to perform all the calculator opertations (check ``src/actions/index.js`` for more information).

## Folder structure.
    .
    ├── index.html
    ├── package.json
    ├── Procfile
    ├── README.md
    ├── resources
    │   ├── local
    │   │   └── stylesheet
    │   │       ├── base
    │   │       │   ├── _constants.scss
    │   │       │   ├── _media-queries.scss
    │   │       │   ├── mixins
    │   │       │   │   ├── _border-radius.scss
    │   │       │   │   └── _transitions.scss
    │   │       │   ├── _mixins.scss
    │   │       │   └── _reset.scss
    │   │       ├── layout
    │   │       │   └── _grid.scss
    │   │       ├── modules
    │   │       │   ├── _animated-button.scss
    │   │       │   ├── _btn.scss
    │   │       │   ├── _calculator.scss
    │   │       │   ├── _footer.scss
    │   │       │   ├── _header.scss
    │   │       │   ├── _icons.scss
    │   │       │   ├── _sections.scss
    │   │       │   ├── _spin.scss
    │   │       │   └── _text.scss
    │   │       └── vendors
    │   │       │   └── _grid.min.scss
    │   │       └── calculator.scss
    │   └── vendors
    │       └── stylesheet
    │           └── normalize.min.css
    ├── server
    │   └── server.js
    ├── src
    │   ├── actions
    │   │   ├── actionTypes.js
    │   │   └── index.js
    │   ├── components
    │   │   ├── App.js
    │   │   ├── AutoScalingText.js
    │   │   ├── CalculatorDisplay.js
    │   │   ├── Calculator.js
    │   │   └── CalculatorKey.js
    │   ├── config
    │   │   ├── common.js
    │   │   └── env.js
    │   ├── index.js
    │   └── reducers
    │       ├── addition.js
    │       ├── index.js
    │       ├── multiplication.js
    │       └── subtraction.js
    ├── webpack.config.js
    └── webpack.production.config.js


## Setup
Get the application code cloning the git repository and installing all the dependencies.

```
$ git clone https://pmagasdev@bitbucket.org/eightdevelopmentinterview/modulo-seven-calculator-ui.git
$ introduce password
$ cd modulo-seven-calculator-ui
$ npm install
```

## Pull up the server (devolopment).

    npm run dev

    Visit http://0.0.0.0:5000/ in your browser.

## Demo

Check how it is working on below link.

    http://modulosevencalculator.pablomagro.co.nz/

## Functionalities implemented.

![Modulo 7 Calculator example][calculator-image]

### Addition

Returns the result of the following operation: ``(a + b) modulus 7``.

### Subtraction

Returns the result of the following operation: ``(a - b) modulus 7``.

### Multiplication

Returns the result of the following operation: ``(a * b) modulus 7``.

### Extra buttons and functionalities

I have included below buttons to make the calculator more intuitive and easier to handle.

``AC``: Clear the calculator display.

``±``: Add sign to the number.

``Backspace``: Same as bottom ``←``.

[calculator-image]: https://c1.staticflickr.com/9/8313/29289693041_1bf6aa66f1_o.png "Modulo 7 Calculator example"
